package ch.codelabs.gitter.test;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.TextView;
import ch.codelabs.gitter.CommitListActivity;

public class CommitListActivityTest extends
		ActivityInstrumentationTestCase2<CommitListActivity> {

	private CommitListActivity mActivity;
	private TextView mView;
	private String resourceString;

	public CommitListActivityTest() {
		super("ch.codelabs.gitter", CommitListActivity.class);
	}

	public CommitListActivityTest(String pkg,
			Class<CommitListActivity> activityClass) {
		super(pkg, activityClass);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mActivity = this.getActivity();
		mView = (TextView) mActivity
				.findViewById(ch.codelabs.gitter.R.id.commit_title_list);
		resourceString = mActivity
				.getString(ch.codelabs.gitter.R.string.no_commits);
	}

	public void testPreconditions() {
		assertNotNull(mView);
	}

//	public void testText() {
//		assertEquals(resourceString, (String) mView.getText());
//	}



}
