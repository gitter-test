/*
 * Copyright (C) 2012 Martin Kempf <mkempf@hsr.ch>
 * Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
 * Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

package ch.codelabs.gitter.test;

import android.database.Cursor;
import android.test.AndroidTestCase;

import ch.codelabs.gitter.Commit;
import ch.codelabs.gitter.CommitDbAdapter;

public class CommitDbAdapterTest extends AndroidTestCase {

	/*
	 * Register tests.
	 */
	public CommitDbAdapterTest() {
		super();
	}

	/*
	 * Insert commit into database.
	 */
	public void testInsertCommit() {
		final String testHash1 = "c669e1928dec2a9251a819ca40f32beed9e280ec";
		final String testMail = "doe@example.com";
		final String testDate = "Thu Mar 22 21:04:37 CET 2012";
		final CommitDbAdapter commitDbAdapter = new CommitDbAdapter(
				getContext());
		commitDbAdapter.open();

		Commit commit = new Commit();
		commit.mTitle = "Update TODO";
		commit.mCommitId = testHash1;
		commit.mAuthor = "John Doe";
		commit.mDate = testDate;
		commit.mEmail = testMail;
		commit.mDiffLink = "http://1.com/abc";
		commit.mContent = "testdata";

		long id = commitDbAdapter.insertCommit(commit);

		final Cursor c = commitDbAdapter.fetchAllCommits();
		assertTrue(c.getCount() > 0);

		c.moveToFirst();
		assertEquals(testHash1,
				c.getString(c.getColumnIndexOrThrow(CommitDbAdapter.KEY_HASH)));
		assertEquals(0,
				c.getInt(c.getColumnIndexOrThrow(CommitDbAdapter.KEY_READ)));
		assertEquals("Update TODO",
				c.getString(c.getColumnIndexOrThrow(CommitDbAdapter.KEY_TITLE)));
		assertEquals("John Doe", c.getString(c
				.getColumnIndexOrThrow(CommitDbAdapter.KEY_AUTHOR)));
		assertEquals(testDate,
				c.getString(c.getColumnIndexOrThrow(CommitDbAdapter.KEY_DATE)));
		assertEquals(testMail,
				c.getString(c.getColumnIndexOrThrow(CommitDbAdapter.KEY_EMAIL)));
		assertEquals("http://1.com/abc", c.getString(c
				.getColumnIndexOrThrow(CommitDbAdapter.KEY_DIFFLINK)));
		assertEquals("testdata", c.getString(c
				.getColumnIndexOrThrow(CommitDbAdapter.KEY_CONTENT)));

		/* Test 'read' status */
		commitDbAdapter.setReadStatus(id, true);

		Cursor i = commitDbAdapter.fetchCommit(id);
		assertEquals(1,
				i.getInt(i.getColumnIndexOrThrow(CommitDbAdapter.KEY_READ)));

		commitDbAdapter.setReadStatus(id, false);
		i = commitDbAdapter.fetchCommit(id);
		assertEquals(0,
				i.getInt(i.getColumnIndexOrThrow(CommitDbAdapter.KEY_READ)));

		/* Test latest commit getter, insert another commit first */
		final String testHash2 = "abca3c29b985d90f271c7b9ea9f095729759799c";
		commit.mCommitId = testHash2;
		id = commitDbAdapter.insertCommit(commit);
		i = commitDbAdapter.getLatestCommit();
		assertTrue(i.getCount() == 1);
		assertEquals(testHash2,
				i.getString(i.getColumnIndexOrThrow(CommitDbAdapter.KEY_HASH)));

		commitDbAdapter.clearDatabase();
		i = commitDbAdapter.fetchAllCommits();
		assertTrue(i.getCount() == 0);

		c.close();
		i.close();
		commitDbAdapter.close();
	}

	public void testFetchUnreadCommits() {
		final String testHash1 = "c669e1928dec2a9251a819ca40f32beed9e280ec";
		final String testMail = "doe@example.com";
		final String testDate = "Thu Mar 22 21:04:37 CET 2012";
		final CommitDbAdapter commitDbAdapter = new CommitDbAdapter(
				getContext());
		commitDbAdapter.open();
		commitDbAdapter.clearDatabase();

		Commit commit = new Commit();
		commit.mTitle = "Update TODO";
		commit.mCommitId = testHash1;
		commit.mAuthor = "John Doe";
		commit.mDate = testDate;
		commit.mEmail = testMail;
		commit.mDiffLink = "http://1.com/abc";
		commit.mContent = "testdata";

		Commit commit2 = new Commit();
		commit2.mTitle = "Update TODO";
		commit2.mCommitId = testHash1;
		commit2.mAuthor = "John Doe";
		commit2.mDate = testDate;
		commit2.mEmail = testMail;
		commit2.mDiffLink = "http://1.com/abc";
		commit2.mContent = "testdata";

		long id = commitDbAdapter.insertCommit(commit);
		id = commitDbAdapter.insertCommit(commit2);
		commitDbAdapter.setReadStatus(id, true);

		final Cursor c = commitDbAdapter.fetchUnreadCommits();
		assertTrue(c.getCount() == 1); // first commit is unread
		c.close();
		commitDbAdapter.close();
	}

	public void testMarkAllAsRead() {
		final String testHash1 = "c669e1928dec2a9251a819ca40f32beed9e280ec";
		final String testMail = "doe@example.com";
		final String testDate = "Thu Mar 22 21:04:37 CET 2012";
		final CommitDbAdapter commitDbAdapter = new CommitDbAdapter(
				getContext());
		commitDbAdapter.open();
		commitDbAdapter.clearDatabase();

		Commit commit = new Commit();
		commit.mTitle = "Update TODO";
		commit.mCommitId = testHash1;
		commit.mAuthor = "John Doe";
		commit.mDate = testDate;
		commit.mEmail = testMail;
		commit.mDiffLink = "http://1.com/abc";
		commit.mContent = "testdata";

		Commit commit2 = new Commit();
		commit2.mTitle = "Update TODO";
		commit2.mCommitId = testHash1;
		commit2.mAuthor = "John Doe";
		commit2.mDate = testDate;
		commit2.mEmail = testMail;
		commit2.mDiffLink = "http://1.com/abc";
		commit2.mContent = "testdata";

		long id = commitDbAdapter.insertCommit(commit);
		id = commitDbAdapter.insertCommit(commit2);
		commitDbAdapter.setReadStatus(id, true);

		int markedAsRead = commitDbAdapter.markAllAsRead();

		final Cursor c = commitDbAdapter.fetchUnreadCommits();
		assertTrue(c.getCount() == 0); // no unread commits anymore
		assertTrue(markedAsRead == 1);
		c.close();
		commitDbAdapter.close();
	}

}
